#!/bin/sh

# Extraction script for:
# Mega Man X Legacy Collection (RXC1.exe)

# Outputs:
# - Mega Man X (Legacy Collection) (Unverified).sfc
# - Mega Man X2 (Legacy Collection) (Unverified).sfc
# - Mega Man X3 (Legacy Collection) (Unverified).sfc
# - Rockman X (Legacy Collection) (Unverified).sfc
# - Rockman X2 (Legacy Collection) (Unverified).sfc
# - Rockman X3 (Legacy Collection) (Unverified).sfc

# Thanks to:
# - s3phir0th115 (github.com/s3phir0th115) for the original Python script

romextract()
{
	dependency_ucon64 || return 1

	echo "Extracting ROMs from RXC1.exe ..."
	# tail for offset, head for ROM size
	tail -c +12109025 "$FILE" | head -c +1572864 \
		> "$SCRIPTID/Rockman X (Legacy Collection) (Unverified).sfc"
	tail -c +14206177 "$FILE" | head -c +1572864 \
		> "$SCRIPTID/Mega Man X (Legacy Collection) (Unverified).sfc"
	tail -c +16303329 "$FILE" | head -c +1572864 \
		> "$SCRIPTID/Rockman X2 (Legacy Collection) (Unverified).sfc"
	tail -c +17876193 "$FILE" | head -c +1572864 \
		> "$SCRIPTID/Mega Man X2 (Legacy Collection) (Unverified).sfc"
	tail -c +19449057 "$FILE" | head -c +2097152 \
		> "$SCRIPTID/Rockman X3 (Legacy Collection) (Unverified).sfc"
	tail -c +21546209 "$FILE" | head -c +2097152 \
		> "$SCRIPTID/Mega Man X3 (Legacy Collection) (Unverified).sfc"

	for rom in "$SCRIPTID"/*.sfc; do
		echo "Regenerating checksum for $(basename "$rom") ..."
		"$UCON64_PATH" --snes --chk --nbak -o="$SCRIPTID" "$rom" > /dev/null 2>&1
	done

	echo "Script $SCRIPTID.sh done"
}
