#!/bin/sh

# Extraction script for:
# Kirby's Dream Collection - Special Edition (USA).iso

# Outputs:
# - Kirby's Dream Land (USA, Europe).gb
# - Kirby's Dream Land 2 (USA, Europe) (SGB Enhanced).gb
# - Kirby 64 - The Crystal Shards (USA).z64
# - Kirby's Adventure (USA) (Rev A).nes
# - Kirby's Dream Land 3 (Virtual Console) (Sound) (Unverified).smc
# - Kirby Super Star (Virtual Console) (Sound) (Unverified).smc

# Requires: wit python2 lzh8.py brrencode3.py snesrestore.py ucon64

romextract()
{
	dependency_wit          || return 1
	dependency_python2      || return 1
	dependency_snesrestore  || return 1
	dependency_ucon64       || return 1

	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" --files=+/files/vc/

	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +1179729 "$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/FC_yume/FC_yume.dol" \
		| head -c +786448 > "$SCRIPTID/Kirby's Adventure (USA) (Rev A).nes"

	"$PYTHON2_PATH" "$SCRIPTDIR/tools/lzh8.py" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/SF_kby3/LZH8SF_kby3.pcm" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/SF_kby3/SF_kby3.pcm"
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/lzh8.py" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/SF_kby3/LZH8SF_kby3.rom" \
		"$SCRIPTID/Kirby's Dream Land 3 (Virtual Console) (Unverified).rom"
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/lzh8.py" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/SF_sdx/LZH8SF_sdx.pcm" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/SF_sdx/SF_sdx.pcm"
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/lzh8.py" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/SF_sdx/LZH8SF_sdx.rom" \
		"$SCRIPTID/Kirby Super Star (Virtual Console) (Unverified).rom"

	echo "Moving files ..."
	#	GB
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/GB_kby1/DMGKYE-0.553" \
		"$SCRIPTID/Kirby's Dream Land (USA, Europe).gb"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/GB_kby2/DMGAKBE0.A66" \
		"$SCRIPTID/Kirby's Dream Land 2 (USA, Europe) (SGB Enhanced).gb"
	# N64
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/64_k64/rom" \
		"$SCRIPTID/Kirby 64 - The Crystal Shards (USA).z64"

	# TODO: GoodTools hashes match files without uCON64 chk fix
	echo "Merging Kirby's Dream Land 3 ..."
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/snesrestore.py" \
		"$SCRIPTID/Kirby's Dream Land 3 (Virtual Console) (Unverified).rom" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/SF_kby3/SF_kby3.pcm" \
		"$SCRIPTID/Kirby's Dream Land 3 (Virtual Console) (Sound) (Unverified).smc"
	echo "Regenerating checksum for Kirby's Dream Land 3 ..."
	"$UCON64_PATH" --snes --chk --nbak -o="$SCRIPTID" \
		"$SCRIPTID/Kirby's Dream Land 3 (Virtual Console) (Sound) (Unverified).smc" > /dev/null 2>&1
	echo "Merging Kirby Super Star ..."
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/snesrestore.py" \
		"$SCRIPTID/Kirby Super Star (Virtual Console) (Unverified).rom" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/files/vc/SF_sdx/SF_sdx.pcm" \
		"$SCRIPTID/Kirby Super Star (Virtual Console) (Sound) (Unverified).smc"
	echo "Regenerating checksum for Kirby Super Star ..."
	"$UCON64_PATH" --snes --chk --nbak -o="$SCRIPTID" \
		"$SCRIPTID/Kirby Super Star (Virtual Console) (Sound) (Unverified).smc" > /dev/null 2>&1

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
