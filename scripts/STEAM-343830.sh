#!/bin/sh

# Extraction script for:
# Déjà Vu II: MacVenture Series (dejavu2.exe)

# Outputs:
# - Deja Vu II.2mg
# - Deja Vu II.dsk

romextract()
{

	echo "Extracting game files from dejavu2.exe ..."
	# tail for offset, head for game file size
	tail -c +598841 "$FILE" | head -c +819272 \
		> "$SCRIPTID/Deja Vu II.2mg"
	tail -c +1418113 "$FILE" | head -c +819200 \
		> "$SCRIPTID/Deja Vu II.dsk"

	echo "Script $SCRIPTID.sh done"
}
