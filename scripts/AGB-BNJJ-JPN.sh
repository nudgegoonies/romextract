#!/bin/sh

# Extraction script for:
# Jajamaru Jr. Denshouki - Jalecolle mo Arisourou (Japan).gba

# Outputs:
# - Exerion (Japan) (Jajamaru Jr. Denshouki) (Unverified).nes
# - Formation Z (Japan) (Jajamaru Jr. Denshouki) (Unverified).nes
# - City Connection (Japan) (Jajamaru Jr. Denshouki) (Unverified).nes
# - Ninja Jajamaru-kun (Japan) (Jajamaru Jr. Denshouki) (Unverified).nes
# - Jajamaru no Daibouken (Japan) (Jajamaru Jr. Denshouki) (Broken).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +31265 "$FILE" | head -c +24592 \
		> "$SCRIPTID/Exerion (Japan) (Jajamaru Jr. Denshouki) (Unverified).nes"
	tail -c +55905 "$FILE" | head -c +24592 \
		> "$SCRIPTID/Formation Z (Japan) (Jajamaru Jr. Denshouki) (Unverified).nes"
	tail -c +80545 "$FILE" | head -c +32784 \
		> "$SCRIPTID/City Connection (Japan) (Jajamaru Jr. Denshouki) (Unverified).nes"
	tail -c +113377 "$FILE" | head -c +32784 \
		> "$SCRIPTID/Ninja Jajamaru-kun (Japan) (Jajamaru Jr. Denshouki) (Unverified).nes"
	# TODO: Jajamaru no Daibouken broken? cmp shows 12495 differing bytes
	tail -c +146209 "$FILE" | head -c +65552 \
		> "$SCRIPTID/Jajamaru no Daibouken (Japan) (Jajamaru Jr. Denshouki) (Broken).nes"

	echo "Script $SCRIPTID.sh done"
}
