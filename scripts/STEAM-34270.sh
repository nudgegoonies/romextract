#!/bin/sh

# Extraction script for:
# SEGA Mega Drive & Genesis Classics (uncompressed ROMs/*)

# Outputs:
# - Alex Kidd in the Enchanted Castle (USA).md
# - Alien Soldier (Europe).md
# - Alien Storm (World) (Unverified).md
# - Altered Beast (USA, Europe).md
# - Bare Knuckle III (Japan).md
# - Bare Knuckle - Ikari no Tekken ~ Streets of Rage (World) (Rev A).md
# - Beyond Oasis (USA).md
# - Bio Hazard Battle (USA, Europe).md
# - Bonanza Bros. (Japan, Europe) (Rev A).md
# - Columns III (USA).md
# - Columns (USA, Europe).md
# - Comix Zone (USA).md
# - Crack Down (USA).md
# - DEcapAttack (USA, Europe, Korea).md
# - Dr. Robotnik's Mean Bean Machine (USA).md
# - Dynamite Headdy (Japan).md
# - Dynamite Headdy (USA, Europe).md
# - Ecco Jr. (USA, Australia).(s)md
# - Ecco the Dolphin (USA, Europe, Korea).md
# - Ecco - The Tides of Time (USA).md
# - ESWAT - City Under Siege (USA, Europe) (Rev A).md
# - Eternal Champions (USA).md
# - Fatal Labyrinth (USA, Europe).md
# - Flicky (USA, Europe).md
# - Gain Ground (USA).md
# - Galaxy Force II (USA, Europe) (Steam Version).md
# - Golden Axe III (Japan) (En).md
# - Golden Axe II (World).md
# - Golden Axe (World) (Rev A).md
# - Gunstar Heroes (USA).md
# - Kid Chameleon (USA, Europe).md
# - Landstalker (USA).md
# - Legende de Thor, La (France).md
# - Light Crusader (USA).md
# - Phantasy Star III - Generations of Doom (USA, Europe) (Steam Version).md
# - Phantasy Star II (USA, Europe) (Steam Version).md
# - Phantasy Star IV (USA) (Unverified).md
# - Revenge of Shinobi, The (World) (Rev C) (Virtual Console).md
# - Ristar (USA, Europe).md
# - Shadow Dancer - The Secret of Shinobi (USA, Europe) (Steam Version).md
# - Shining Force II (USA).md
# - Shining Force (USA).md
# - Shining in the Darkness (USA, Europe).md
# - Shinobi III - Return of the Ninja Master (USA).md
# - Sonic 3D Blast ~ Sonic 3D Flickies' Island (USA, Europe, Korea).md
# - Sonic & Knuckles (World).md
# - Sonic & Knuckles + Sonic The Hedgehog 3 (Japan) (En).md
# - Sonic Spinball (USA).md
# - Sonic The Hedgehog 2 (World) (Rev A).md
# - Sonic The Hedgehog 3 (Japan, Korea).md
# - Sonic The Hedgehog (Japan, Korea).md
# - Space Harrier II (World).md
# - Story of Thor, The (Europe).md
# - Story of Thor, The (Germany).md
# - Story of Thor, The - Hikari o Tsugumono (Japan).md
# - Story of Thor, The (Spain).md
# - Streets of Rage 2 (USA).md
# - Streets of Rage 3 (Asia, Korea).md
# - Streets of Rage 3 (USA).md
# - Super Thunder Blade (World) (Rev A).md
# - Sword of Vermilion (USA, Europe).(s)md
# - ToeJam & Earl in Panic on Funkotron (USA).md
# - ToeJam & Earl (Japan, Europe) (Rev A).md
# - Vectorman 2 (USA).md
# - Vectorman (USA, Europe).md
# - Virtua Fighter 2 (USA, Europe).md
# - Wonder Boy III - Monster Lair (Japan, Europe).md
# - Wonder Boy in Monster World (USA, Europe).md
# - Wonder Boy V - Monster World III (Japan, Korea).md

# Requires: ucon64 (soft)

#TODO Add support for unique ROMs from pak archives
# g0015.pak: Ecco Jr. (USA, Australia).md
# g0032.pak: Sword of Vermilion (USA, Europe).md
# g0052.pak: Phantasy Star III - Generations of Doom (USA, Europe, Korea).md
# g0053.pak: Phantasy Star IV (USA).md
# g0055.pak: Revenge of Shinobi, The (USA, Europe).md
# g9001.pak: Alien Soldier (Japan).md
# g9007.pak: Landstalker - The Treasures of King Nole (Europe) (Unverified).md
# g9008.pak: Landstalker - Le Tresor du Roi Nole (France).md
# g9009.pak: Landstalker - Die Schatze von Konig Nolo (Germany).md
# g9010.pak: Ristar - The Shooting Star (Japan, Korea).md
# g9011.pak: Bare Knuckle II - Shitou e no Requiem ~ Streets of Rage II (Japan, Europe).md

romextract()
{
	SRCNAME="ALEXKIDD_U.68K:AlienSoldier_Europe.SGD:AlienStorm_USA.SGD:ALTEREDB_UE.68K:BEYONDOA_E.68K:BEYONDOA_F.68K:BEYONDOA_G.68K:BEYONDOA_J.68K:BEYONDOA_S.68K:BEYONDOA_U.68K:BONANZAB_JE.68K:Columns3_USA.SGD:COLUMNS_W.68K:COMIXZON_U.68K:CrackDown_USA.SGD:Crying_USA.SGD:DECAP_UE.68K:DYNAHEAD_J.68K:DYNAHEAD_UE.68K:ECCO2_U.68K:eccojr.smd:ECCO_UE.68K:ESWAT_U.68K:EternalChampions_USA.SGD:FATALLAB_JU.68K:FLICKY_UE.68K:GAING_UE.68K:GalaxyForceII_UE.SGD:GAXE2_W.68K:GAXE3_J.68K:GAXE_W.68K:Gunstar Heroes U.bin:KIDCHAM_UE.68K:LandStalker_USA.SGD:LightCrusader_USA.SGD:MonsterLair_JUE.SGD:MonsterWorld3.SGD:MonsterWorld3_USA.SGD:PhantasyStar2_UE_GreenCrossFix.SGD:PhantasyStar3_USA.SGD:PhantasyStar4.SGD:RISTAR_UE.68K:ROBOTNIK_U.68K:ShadowDancer.SGD:SHINING2_U.68K:SHININGD_UE.68K:SHININGF_U.68K:SHINOBI3_U.68K:SONIC2_W.68K:SONIC3D_UE.68K:Sonic_Knuckles_wSonic3.bin:SONICSPI_U.68K:SONIC_W.68K:sov.smd:SPACEHARRIERII.bin:STHUNDER_W.68K:STREETS2_U.68K:STREETS3_E.68K:STREETS3_J.68K:STREETS3_U.68K:STREETS_W.68K:TheSuperShinobi_JUE.SGD:ToeJamEarl2_USA.SGD:ToeJamEarl.SGD:VECTMAN2_U.68K:VECTMAN_UE.68K:VIRTUAFIGHTER2.bin:"
	DESTNAME="Alex Kidd in the Enchanted Castle (USA).md:Alien Soldier (Europe).md:Alien Storm (World) (Unverified).md:Altered Beast (USA, Europe).md:Story of Thor, The (Europe).md:Legende de Thor, La (France).md:Story of Thor, The (Germany).md:Story of Thor, The - Hikari o Tsugumono (Japan).md:Story of Thor, The (Spain).md:Beyond Oasis (USA).md:Bonanza Bros. (Japan, Europe) (Rev A).md:Columns III (USA).md:Columns (USA, Europe).md:Comix Zone (USA).md:Crack Down (USA).md:Bio Hazard Battle (USA, Europe).md:DEcapAttack (USA, Europe, Korea).md:Dynamite Headdy (Japan).md:Dynamite Headdy (USA, Europe).md:Ecco - The Tides of Time (USA).md:Ecco Jr. (USA, Australia).smd:Ecco the Dolphin (USA, Europe, Korea).md:ESWAT - City Under Siege (USA, Europe) (Rev A).md:Eternal Champions (USA).md:Fatal Labyrinth (USA, Europe).md:Flicky (USA, Europe).md:Gain Ground (USA).md:Galaxy Force II (USA, Europe) (Steam Version).md:Golden Axe II (World).md:Golden Axe III (Japan) (En).md:Golden Axe (World) (Rev A).md:Gunstar Heroes (USA).md:Kid Chameleon (USA, Europe).md:Landstalker (USA).md:Light Crusader (USA).md:Wonder Boy III - Monster Lair (Japan, Europe).md:Wonder Boy V - Monster World III (Japan, Korea).md:Wonder Boy in Monster World (USA, Europe).md:Phantasy Star II (USA, Europe) (Steam Version).md:Phantasy Star III - Generations of Doom (USA, Europe) (Steam Version).md:Phantasy Star IV (USA) (Unverified).md:Ristar (USA, Europe).md:Dr. Robotnik's Mean Bean Machine (USA).md:Shadow Dancer - The Secret of Shinobi (USA, Europe) (Steam Version).md:Shining Force II (USA).md:Shining in the Darkness (USA, Europe).md:Shining Force (USA).md:Shinobi III - Return of the Ninja Master (USA).md:Sonic The Hedgehog 2 (World) (Rev A).md:Sonic 3D Blast ~ Sonic 3D Flickies' Island (USA, Europe, Korea).md:Sonic & Knuckles + Sonic The Hedgehog 3 (Japan) (En).md:Sonic Spinball (USA).md:Sonic The Hedgehog (Japan, Korea).md:Sword of Vermilion (USA, Europe).smd:Space Harrier II (World).md:Super Thunder Blade (World) (Rev A).md:Streets of Rage 2 (USA).md:Streets of Rage 3 (Asia, Korea).md:Bare Knuckle III (Japan).md:Streets of Rage 3 (USA).md:Bare Knuckle - Ikari no Tekken ~ Streets of Rage (World) (Rev A).md:Revenge of Shinobi, The (World) (Rev C) (Virtual Console).md:ToeJam & Earl in Panic on Funkotron (USA).md:ToeJam & Earl (Japan, Europe) (Rev A).md:Vectorman 2 (USA).md:Vectorman (USA, Europe).md:Virtua Fighter 2 (USA, Europe).md:"

	DIR="$(dirname "$FILE")/uncompressed ROMs/"

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID" || return 1

	echo "Copying/Converting uncompressed ROMs ..."

	while [ -n "$SRCNAME" ] &&	[ -n "$DESTNAME" ]; do
		TMPSRCNAME="${SRCNAME%%:*}"
		TMPDESTNAME="${DESTNAME%%:*}"
		# Only copy/convert if source exists
		if ! [ -f "$DIR/$TMPSRCNAME" ]; then
			continue
		fi
		# convert SMD ROMs when ucon64 is installed, otherwise just copy them
		if [ "${TMPDESTNAME##*.}" = "smd" ] && dependency_ucon64 > /dev/null 2>&1; then
			echo "Converting ${TMPDESTNAME%.smd} from SMD to raw"
			"$UCON64_PATH" --gen --bin --nbak -o="$ROMEXTRACT_TMPDIR/$SCRIPTID" \
				"$DIR/$TMPSRCNAME" > /dev/null 2>&1
			mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/${TMPSRCNAME%.*}.bin" \
				"$SCRIPTID/${TMPDESTNAME%.smd}.md" 
		else
			cp "$DIR/$TMPSRCNAME" "$SCRIPTID/$TMPDESTNAME"
		fi
		SRCNAME=${SRCNAME#*:}
		DESTNAME=${DESTNAME#*:}
	done

	if [ -f "$SCRIPTID/Sonic & Knuckles + Sonic The Hedgehog 3 (Japan) (En).md" ]; then
		tail -c +2097153 "$SCRIPTID/Sonic & Knuckles + Sonic The Hedgehog 3 (Japan) (En).md" \
			> "$SCRIPTID/Sonic The Hedgehog 3 (Japan, Korea).md"
		head -c +2097152	"$SCRIPTID/Sonic & Knuckles + Sonic The Hedgehog 3 (Japan) (En).md" \
			> "$SCRIPTID/Sonic & Knuckles (World).md"
	fi

	echo "Cleaning up ..."
	chmod 0644 "$SCRIPTID/"*
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
