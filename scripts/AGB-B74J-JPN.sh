#!/bin/sh

# Extraction script for:
# Hudson Best Collection Vol. 4 - Nazotoki Collection (Japan).gba

# Outputs:
# - Nuts & Milk (Japan) (Hudson Best Collection) (Unverified).nes
# - Binary Land (Japan) (Hudson Best Collection) (Unverified).nes
# - Salad no Kuni no Tomato Hime (Japan) (Hudson Best Collection) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +184021 "$FILE" | head -c +24592 \
		> "$SCRIPTID/Nuts & Milk (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +208613 "$FILE" | head -c +24592 \
		> "$SCRIPTID/Binary Land (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +233205 "$FILE" | head -c +262160 \
		> "$SCRIPTID/Salad no Kuni no Tomato Hime (Japan) (Hudson Best Collection) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
