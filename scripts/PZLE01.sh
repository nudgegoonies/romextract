#!/bin/sh

# Extraction script for:
# Legend of Zelda, The - Collector's Edition (USA).iso

# Outputs:
# - Legend of Zelda, The - Majoras Mask (USA) (Collectors Edition).z64
# - Legend of Zelda, The - Ocarina of Time (USA) (Collectors Edition).z64
# - Legend of Zelda, The - The Wind Waker Demo (USA).tgc
# - Legend of Zelda, The (USA) (Collectors Edition).nes
# - Zelda II - The Adventure of Link (USA) (Collectors Edition).nes

# Requires: wit

romextract()
{
	dependency_wit || return 1

	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" \
		--files=+/files/tgc/USA_NES_ZELDA1.tgc \
		--files=+/files/tgc/USA_NES_ZELDA2.tgc \
		--files=+/files/tgc/majora_ENG_091003.tgc \
		--files=+/files/tgc/zelda_ENG_090903.tgc \
		--files=+/files/tgc/ZL_WindWakerUSASHOP_final_2003-09-08_16-30-56.tgc

	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +543201 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLE/files/tgc/USA_NES_ZELDA1.tgc" \
		| head -c +131088 > "$SCRIPTID/Legend of Zelda, The (USA) (Collector's Edition).nes"
	tail -c +542945 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLE/files/tgc/USA_NES_ZELDA2.tgc" \
		| head -c +262160 > "$SCRIPTID/Zelda II - The Adventure of Link (USA) (Collector's Edition).nes"
	tail -c +25010113 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLE/files/tgc/majora_ENG_091003.tgc" \
		| head -c +33554432 > "$SCRIPTID/Legend of Zelda, The - Majora's Mask (USA) (Collector's Edition).z64"
	tail -c +478445569 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLE/files/tgc/zelda_ENG_090903.tgc" \
		| head -c +33554432 > "$SCRIPTID/Legend of Zelda, The - Ocarina of Time (USA) (Collector's Edition).z64"
	#tail -c +5045781 "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLE/files/tgc/zelda_ENG_090903.tgc" \
	#	| head -c +466727296 > "$SCRIPTID/Legend of Zelda, The - Ocarina of Time Credits (USA) (Collector's Edition).thp"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-PZLE/files/tgc/ZL_WindWakerUSASHOP_final_2003-09-08_16-30-56.tgc" \
		"$SCRIPTID/Legend of Zelda, The - The Wind Waker Demo (USA).tgc"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
