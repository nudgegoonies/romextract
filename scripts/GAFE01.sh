#!/bin/sh

# Extraction script for:
# Animal Crossing (Europe) (En,Fr,De,Es,It).iso

# Outputs:
# - Animal Crossing - Balloon Fight (USA, Europe).gba
# - Animal Crossing - Baseball (USA, Europe).gba
# - Animal Crossing - Clu Clu Land (USA, Europe).gba
# - Animal Crossing - Donkey Kong 3 (USA, Europe).gba
# - Animal Crossing - Donkey Kong Jr. Math (USA, Europe).gba
# - Animal Crossing - Donkey Kong Jr. (USA, Europe).gba
# - Animal Crossing - Donkey Kong (USA, Europe).gba
# - Animal Crossing - Excitebike (USA, Europe).gba
# - Animal Crossing - Golf (USA, Europe).gba
# - Animal Crossing - Ice Climber (USA, Europe).gba
# - Animal Crossing - Mario Bros. (USA, Europe).gba
# - Animal Crossing - Pinball (USA, Europe).gba
# - Animal Crossing - Soccer (USA, Europe).gba
# - Animal Crossing - Super Mario Bros. (USA, Europe).gba
# - Animal Crossing - Tennis (USA, Europe).gba
# - [BIOS] Family Computer Disk System (Japan) (Rev 1).bin
# - Balloon Fight (USA).nes
# - Baseball (USA, Europe) (GameCube Edition).nes
# - Clu Clu Land (Japan) (GameCube, Virtual Console).qd
# - Clu Clu Land (World) (GameCube Edition).nes
# - Donkey Kong 3 (World).nes
# - Donkey Kong Jr. Math (USA, Europe).nes
# - Donkey Kong Jr. (World) (Rev 1) (GameCube Edition).nes
# - Donkey Kong (World) (Rev 1) (GameCube Edition).nes
# - Excitebike (Japan, USA) (GameCube Edition).nes
# - Golf (USA).nes
# - Ice Climber (USA, Europe, Korea).nes
# - Legend of Zelda, The (USA) (Rev 1) (GameCube Edition).nes
# - Mario Bros. (World) (GameCube Edition).nes
# - Pinball (Japan, USA) (GameCube Edition).nes
# - Punch-Out!! (USA).nes
# - Soccer (Japan, USA).nes
# - Super Mario Bros. (World).nes
# - Tennis (Japan, USA) (GameCube Edition).nes
# - Warios Woods (USA).nes

# Requires: wit, wszst, yaz0dec

# Thanks to /u/I_want_FDS_BIOS on Reddit for their guide on extracting these
# files: https://www.reddit.com/r/emulation/comments/377gug/

romextract()
{
	dependency_wit      || return 1
	dependency_wszst    || return 1
	dependency_yaz0dec  || return 1

	echo "Extracting file from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" \
	--files=+/files/famicom.arc

	# Unpack RARC archive
	"$WSZST_PATH" X "$ROMEXTRACT_TMPDIR/$SCRIPTID/P-GAFE/files/famicom.arc" \
		-d "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d"

	echo "Decoding FDS BIOS ..."
	"$YAZ0DEC_PATH" "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs"
	# Truncate and patch BIOS to match verified dump
	truncate -s 8192 "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc"
	# Octal for Dash/POSIX compatibility
	printf '\205' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" count=1 bs=1 seek=$((0x239)) conv=notrunc
	printf '\205' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" count=1 bs=1 seek=$((0x406)) conv=notrunc
	printf '\242' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" count=1 bs=1 seek=$((0x73e)) conv=notrunc
	printf '\262' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" count=1 bs=1 seek=$((0x73f)) conv=notrunc
	printf '\312' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" count=1 bs=1 seek=$((0x740)) conv=notrunc
	printf '\114' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" count=1 bs=1 seek=$((0x7a4)) conv=notrunc
	printf '\245' | dd status=none of="$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" count=1 bs=1 seek=$((0xef4)) conv=notrunc

	echo "Decoding NES ROMs ..."
	for rom in "$ROMEXTRACT_TMPDIR/$SCRIPTID"/famicom.d/game/01/*.szs; do
		"$YAZ0DEC_PATH" "$rom" > /dev/null 2>&1
	done

	echo "Decoding GBA ROMs ..."
	for rom in "$ROMEXTRACT_TMPDIR/$SCRIPTID"/famicom.d/gba/*.szs; do
		"$YAZ0DEC_PATH" "$rom" > /dev/null 2>&1
	done

	echo "Moving files ..."
	# FDS BIOS
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/noise.bin.szs 0.rarc" \
		"$SCRIPTID/[BIOS] Family Computer Disk System (Japan) (Rev 1).bin"
	# NES
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/01_nes_cluclu3.bin.szs 0.rarc" \
		"$SCRIPTID/Clu Clu Land (World) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/02_usa_balloon.nes.szs 0.rarc" \
		"$SCRIPTID/Balloon Fight (USA).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/03_nes_donkey1_3.bin.szs 0.rarc" \
		"$SCRIPTID/Donkey Kong (World) (Rev 1) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/04_usa_jr_math.nes.szs 0.rarc" \
		"$SCRIPTID/Donkey Kong Jr. Math (USA, Europe).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/05_pinball_1.nes.szs 0.rarc" \
		"$SCRIPTID/Pinball (Japan, USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/06_nes_tennis3.bin.szs 0.rarc" \
		"$SCRIPTID/Tennis (Japan, USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/07_usa_golf.nes.szs 0.rarc" \
		"$SCRIPTID/Golf (USA).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/08_punch_wh.nes.szs 0.rarc" \
		"$SCRIPTID/Punch-Out!! (USA).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/09_usa_baseball_1.nes.szs 0.rarc" \
		"$SCRIPTID/Baseball (USA, Europe) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/10_cluclu_1.qd.szs 0.rarc" \
		"$SCRIPTID/Clu Clu Land (Japan) (GameCube, Virtual Console).qd"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/11_usa_donkey3.nes.szs 0.rarc" \
		"$SCRIPTID/Donkey Kong 3 (World).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/12_donkeyjr_1.nes.szs 0.rarc" \
		"$SCRIPTID/Donkey Kong Jr. (World) (Rev 1) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/13_soccer.nes.szs 0.rarc" \
		"$SCRIPTID/Soccer (Japan, USA).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/14_exbike.nes.szs 0.rarc" \
		"$SCRIPTID/Excitebike (Japan, USA) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/15_usa_wario.nes.szs 0.rarc" \
		"$SCRIPTID/Wario's Woods (USA).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/16_usa_icecl.nes.szs 0.rarc" \
		"$SCRIPTID/Ice Climber (USA, Europe, Korea).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/17_nes_mario1_2.bin.szs 0.rarc" \
		"$SCRIPTID/Mario Bros. (World) (GameCube Edition).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/18_smario_0.nes.szs 0.rarc" \
		"$SCRIPTID/Super Mario Bros. (World).nes"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/game/01/19_usa_zelda1_1.nes.szs 0.rarc" \
		"$SCRIPTID/Legend of Zelda, The (USA) (Rev 1) (GameCube Edition).nes"
	# GBA
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_cluclu.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Clu Clu Land (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_donkey.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Donkey Kong (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_donkeyjr.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Donkey Kong Jr. (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_exbike.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Excitebike (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_mario.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Mario Bros. (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_pinball.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Pinball (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_smario.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Super Mario Bros. (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_soccer.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Soccer (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_tennis.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Tennis (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_balloon.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Balloon Fight (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_baseball.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Baseball (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_donkey3.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Donkey Kong 3 (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_golf.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Golf (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_icecl.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Ice Climber (USA, Europe).gba"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/famicom.d/gba/jb_usa_jr_math.bin.szs 0.rarc" \
		 "$SCRIPTID/Animal Crossing - Donkey Kong Jr. Math (USA, Europe).gba"

	# TODO: Extract Animal Island ROM if possible?
	#       Most likely inside `foresta.rel.szs` which matches:
	#       grep -rl $(printf "\xEA\x24\xFF\xAE\x51\x69\x9A\xA2\x21\x3D")

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
